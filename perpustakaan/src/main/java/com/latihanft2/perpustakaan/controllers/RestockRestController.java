package com.latihanft2.perpustakaan.controllers;

import com.latihanft2.perpustakaan.models.Restock;
import com.latihanft2.perpustakaan.repositories.RestockRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("api")
public class RestockRestController {

    @Autowired
    private RestockRepo restockRepo;

    @GetMapping("getallrestock")
    public ResponseEntity<List<Restock>> GetAllRestock()
    {
        try {
            List<Restock> restock = this.restockRepo.findAllNotDeleted();
            return new ResponseEntity<>(restock, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("addrestock")
    public ResponseEntity<Object> AddRestock(@RequestBody Restock restock)
    {
        try
        {
            this.restockRepo.save(restock);
            return new ResponseEntity<>(restock,HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("getbyidrestock/{id}")
    public ResponseEntity<List<Restock>> GetRestockById(@PathVariable("id") Long id)
    {
        try {
            Optional<Restock> restock = this.restockRepo.findById(id);

            if (restock.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(restock, HttpStatus.OK);
                return rest;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("editrestock/{id}")
    public ResponseEntity<Object> EditRestock(@RequestBody Restock restock, @PathVariable("id") Long id)
    {
        Optional<Restock> restockData = this.restockRepo.findById(id);

        if (restockData.isPresent())
        {
            restock.setIdRestock(id);
            this.restockRepo.save(restock);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deleterestock/{id}")
    public String DeleteRestock(@PathVariable("id") Long id)
    {
        try {
            Restock restock = this.restockRepo.findByIdData(id);
            restock.setDelete(true);
            this.restockRepo.save(restock);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
