package com.latihanft2.perpustakaan.controllers;

import com.latihanft2.perpustakaan.models.Penulis;
import com.latihanft2.perpustakaan.repositories.PenulisRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api")
public class PenulisRestController {
    @Autowired
    private PenulisRepo penulisRepo;

    @GetMapping("getallpenulis")
    public ResponseEntity<List<Penulis>> GetAllPenulis()
    {
        try {
            List<Penulis> penulis = this.penulisRepo.findAllNotDeleted();
            return new ResponseEntity<>(penulis, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("addpenulis")
    public ResponseEntity<Object> AddPenulis(@RequestBody Penulis penulis)
    {
        try
        {
//            penulis.setCreatedBy("ZoeZoe");
//            penulis.setCreatedOn(new Date());
            this.penulisRepo.save(penulis);
            return new ResponseEntity<>(penulis,HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("getbyidpenulis/{id}")
    public ResponseEntity<List<Penulis>> GetPenulisById(@PathVariable("id") Long id)
    {
        try {
            Optional<Penulis> penulis = this.penulisRepo.findById(id);

            if (penulis.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(penulis, HttpStatus.OK);
                return rest;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("editpenulis/{id}")
    public ResponseEntity<Object> EditPenulis(@RequestBody Penulis penulis, @PathVariable("id") Long id)
    {
        Optional<Penulis> penulisData = this.penulisRepo.findById(id);

        if (penulisData.isPresent())
        {
            penulis.setIdPenulis(id);
            this.penulisRepo.save(penulis);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletepenulis/{id}")
    public String DeletePenulis(@PathVariable("id") Long id)
    {
        try {
            Penulis penulis = this.penulisRepo.findByIdData(id);
            penulis.setDelete(true);
            this.penulisRepo.save(penulis);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

    @GetMapping("penulismapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5")int size)
    {
        try{
            List <Penulis> penulis = this.penulisRepo.findAllNotDeleted();

            int start = page * size;
            int end = Math.min((start + size), penulis.size());

            List<Penulis> paginatedPenulis = penulis.subList(start, end);

            Map<String, Object> response = new HashMap<>();
            response.put("penulis", paginatedPenulis);
            response.put("currentPage", page);
            response.put("totalItems", penulis.size());
            response.put("totalPages",(int)Math.ceil((double) penulis.size() / size));

            return new ResponseEntity<>(response,HttpStatus.OK);
        }

        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/searchpenulis/{keyword}")
    public ResponseEntity<List<Penulis>> SearchPenulisName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Penulis> penulis = this.penulisRepo.SearchPenulis(keyword);
            return new ResponseEntity<>(penulis, HttpStatus.OK);
        } else {
            List<Penulis> penulis = this.penulisRepo.findAllNotDeleted();
            return new ResponseEntity<>(penulis, HttpStatus.OK);
        }
    }
}
