package com.latihanft2.perpustakaan.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("penerbit")
public class PenerbitController {
    @RequestMapping("")
    public String penerbit(){
        return "penerbit/penerbit";
    }

    @RequestMapping("addpenerbit")
    public String addPenerbit(){
        return "penerbit/addpenerbit";
    }

    @RequestMapping("editpenerbit/{id}")
    public String editPenerbit(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "penerbit/editpenerbit";
    }

    @RequestMapping("deletepenerbit/{id}")
    public String deletePenerbit(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "penerbit/deletepenerbit";
    }
}
