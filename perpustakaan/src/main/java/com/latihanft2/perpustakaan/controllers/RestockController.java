package com.latihanft2.perpustakaan.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("restock")
public class RestockController {
    @RequestMapping("")
    public String restock() {
        return "restock/restock";
    }
}
