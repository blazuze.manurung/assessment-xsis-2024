package com.latihanft2.perpustakaan.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping("buku")
public class BukuController {

    @RequestMapping("")
    public String buku(){
        return "buku/buku";
    }

    @RequestMapping("addbuku")
    public String addBuku(){
        return "buku/addbuku";
    }

    @RequestMapping("editbuku/{id}")
    public String editBuku(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "buku/editbuku";
    }

    @RequestMapping("deletebuku/{id}")
    public String deleteBuku(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "buku/deletebuku";
    }
}
