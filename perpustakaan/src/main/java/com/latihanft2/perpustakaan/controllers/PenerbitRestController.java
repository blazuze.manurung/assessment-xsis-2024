package com.latihanft2.perpustakaan.controllers;

import com.latihanft2.perpustakaan.models.Penerbit;
import com.latihanft2.perpustakaan.repositories.PenerbitRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api")
public class PenerbitRestController {
    @Autowired
    private PenerbitRepo penerbitRepo;

    @GetMapping("getallpenerbit")
    public ResponseEntity<List<Penerbit>> GetAllPenerbit()
    {
        try {
            List<Penerbit> penerbit = this.penerbitRepo.findAllNotDeleted();
            return new ResponseEntity<>(penerbit, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("addpenerbit")
    public ResponseEntity<Object> AddPenerbit(@RequestBody Penerbit penerbit)
    {
        try
        {
            this.penerbitRepo.save(penerbit);
            return new ResponseEntity<>(penerbit,HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("getbyidpenerbit/{id}")
    public ResponseEntity<List<Penerbit>> GetPenerbitById(@PathVariable("id") Long id)
    {
        try {
            Optional<Penerbit> penerbit = this.penerbitRepo.findById(id);

            if (penerbit.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(penerbit, HttpStatus.OK);
                return rest;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("editpenerbit/{id}")
    public ResponseEntity<Object> EditPenerbit(@RequestBody Penerbit penerbit, @PathVariable("id") Long id)
    {
        Optional<Penerbit> penerbitData = this.penerbitRepo.findById(id);

        if (penerbitData.isPresent())
        {
            penerbit.setIdPenerbit(id);
            this.penerbitRepo.save(penerbit);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletepenerbit/{id}")
    public String DeletePenerbit(@PathVariable("id") Long id)
    {
        try {
            Penerbit penerbit = this.penerbitRepo.findByIdData(id);
            penerbit.setDelete(true);
            this.penerbitRepo.save(penerbit);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

    @GetMapping("penerbitmapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5")int size)
    {
        try{
            List <Penerbit> penerbit = this.penerbitRepo.findAllNotDeleted();

            int start = page * size;
            int end = Math.min((start + size), penerbit.size());

            List<Penerbit> paginatedPenerbit = penerbit.subList(start, end);

            Map<String, Object> response = new HashMap<>();
            response.put("penerbit", paginatedPenerbit);
            response.put("currentPage", page);
            response.put("totalItems", penerbit.size());
            response.put("totalPages",(int)Math.ceil((double) penerbit.size() / size));

            return new ResponseEntity<>(response,HttpStatus.OK);
        }

        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/searchpenerbit/{keyword}")
    public ResponseEntity<List<Penerbit>> SearchPenerbitName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<Penerbit> penerbit = this.penerbitRepo.SearchPenerbit(keyword);
            return new ResponseEntity<>(penerbit, HttpStatus.OK);
        } else {
            List<Penerbit> penerbit = this.penerbitRepo.findAllNotDeleted();
            return new ResponseEntity<>(penerbit, HttpStatus.OK);
        }
    }
}
