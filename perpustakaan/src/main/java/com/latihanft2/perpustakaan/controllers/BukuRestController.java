package com.latihanft2.perpustakaan.controllers;

import com.latihanft2.perpustakaan.models.Buku;
import com.latihanft2.perpustakaan.models.Penerbit;
import com.latihanft2.perpustakaan.repositories.BukuRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("api")
public class BukuRestController {
    @Autowired
    private BukuRepo bukuRepo;

    @GetMapping("getallbuku")
    public ResponseEntity<List<Buku>> GetAllBuku()
    {
        try {
            List<Buku> buku = this.bukuRepo.findAllNotDeleted();
            return new ResponseEntity<>(buku, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("addbuku")
    public ResponseEntity<Object> AddBuku(@RequestBody Buku buku)
    {
        try
        {
            this.bukuRepo.save(buku);
            return new ResponseEntity<>(buku,HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("getbyidbuku/{id}")
    public ResponseEntity<List<Buku>> GetBukuById(@PathVariable("id") Long id)
    {
        try {
            Optional<Buku> buku = this.bukuRepo.findById(id);

            if (buku.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(buku, HttpStatus.OK);
                return rest;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("editbuku/{id}")
    public ResponseEntity<Object> EditBuku(@RequestBody Buku buku, @PathVariable("id") Long id)
    {
        Optional<Buku> bukuData = this.bukuRepo.findById(id);

        if (bukuData.isPresent())
        {
            buku.setIdBuku(id);
            this.bukuRepo.save(buku);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletebuku/{id}")
    public String DeleteBuku(@PathVariable("id") Long id)
    {
        try {
            Buku buku = this.bukuRepo.findByIdData(id);
            buku.setDelete(true);
            this.bukuRepo.save(buku);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

    @GetMapping("bukumapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5")int size)
    {
        try{
            List <Buku> buku = this.bukuRepo.findAllNotDeleted();

            int start = page * size;
            int end = Math.min((start + size), buku.size());

            List<Buku> paginatedBuku = buku.subList(start, end);

            Map<String, Object> response = new HashMap<>();
            response.put("buku", paginatedBuku);
            response.put("currentPage", page);
            response.put("totalItems", buku.size());
            response.put("totalPages",(int)Math.ceil((double) buku.size() / size));

            return new ResponseEntity<>(response,HttpStatus.OK);
        }

        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/searchbuku/{keyword}")
    public ResponseEntity<List<Buku>> SearchBukuName(@PathVariable("keyword") String keyword) {
        if (keyword != null) {
            List<Buku> buku = this.bukuRepo.SearchBuku(keyword);
            return new ResponseEntity<>(buku, HttpStatus.OK);
        } else {
            List<Buku> buku = this.bukuRepo.findAllNotDeleted();
            return new ResponseEntity<>(buku, HttpStatus.OK);
        }
    }
}
