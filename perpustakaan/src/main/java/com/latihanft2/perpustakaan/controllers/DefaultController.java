package com.latihanft2.perpustakaan.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultController {
    @RequestMapping("")
    public String Dashboard()
    {
        return "dashboard";
    }
}
