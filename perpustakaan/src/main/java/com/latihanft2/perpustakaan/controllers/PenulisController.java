package com.latihanft2.perpustakaan.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("penulis")
public class PenulisController {
    @RequestMapping("")
    public String penulis(){
        return "penulis/penulis";
    }

    @RequestMapping("addpenulis")
    public String addPenulis(){
        return "penulis/addpenulis";
    }

    @RequestMapping("editpenulis/{id}")
    public String editPenulis(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "penulis/editpenulis";
    }

    @RequestMapping("deletepenulis/{id}")
    public String deletePenulis(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "penulis/deletepenulis";
    }
}
