package com.latihanft2.perpustakaan.repositories;

import com.latihanft2.perpustakaan.models.Buku;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BukuRepo extends JpaRepository<Buku, Long> {
    @Query(value = "SELECT * FROM buku WHERE isdelete = false", nativeQuery = true)
    List<Buku> findAllNotDeleted();

    @Query(value = "SELECT * FROM buku WHERE id_buku = :id", nativeQuery = true)
    Buku findByIdData(long id);

    @Query(value = "FROM Buku WHERE lower(JudulBuku) LIKE lower(concat('%',?1,'%')) AND isDelete = false")
    List<Buku> SearchBuku(String keyword);
}
