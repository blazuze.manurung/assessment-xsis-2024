package com.latihanft2.perpustakaan.repositories;

import com.latihanft2.perpustakaan.models.Penerbit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PenerbitRepo extends JpaRepository<Penerbit, Long> {
    @Query(value = "SELECT * FROM penerbit WHERE isdelete = false", nativeQuery = true)
    List<Penerbit> findAllNotDeleted();

    @Query(value = "SELECT * FROM penerbit WHERE id_penerbit = :id", nativeQuery = true)
    Penerbit findByIdData(long id);

    @Query(value = "FROM Penerbit WHERE lower(NamaPenerbit) LIKE lower(concat('%',?1,'%')) AND isDelete = false")
    List<Penerbit> SearchPenerbit(String keyword);

}
