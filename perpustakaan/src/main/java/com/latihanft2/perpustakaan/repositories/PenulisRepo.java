package com.latihanft2.perpustakaan.repositories;

import com.latihanft2.perpustakaan.models.Penulis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PenulisRepo extends JpaRepository<Penulis,Long> {

    @Query(value = "SELECT * FROM penulis WHERE isdelete = false", nativeQuery = true)
    List<Penulis> findAllNotDeleted();

    @Query(value = "SELECT * FROM penulis WHERE id_penulis = :id", nativeQuery = true)
    Penulis findByIdData(long id);

    @Query(value = "FROM Penulis WHERE lower(NamaPenulis) LIKE lower(concat('%',?1,'%')) AND isDelete = false")
    List<Penulis> SearchPenulis(String keyword);
}
