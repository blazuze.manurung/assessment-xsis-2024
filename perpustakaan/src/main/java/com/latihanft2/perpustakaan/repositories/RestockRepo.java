package com.latihanft2.perpustakaan.repositories;

import com.latihanft2.perpustakaan.models.Restock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RestockRepo extends JpaRepository<Restock, Long> {
    @Query(value = "SELECT * FROM restock WHERE isdelete = false", nativeQuery = true)
    List<Restock> findAllNotDeleted();
    @Query(value = "SELECT * FROM restock WHERE id_restock = :id", nativeQuery = true)
    Restock findByIdData(long id);
}
