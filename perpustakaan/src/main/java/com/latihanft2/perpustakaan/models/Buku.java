package com.latihanft2.perpustakaan.models;

import javax.persistence.*;

@Entity
@Table (name = "buku")
public class Buku extends CommonEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_buku", nullable = false)
    private long IdBuku;

    @Column(name = "judul_buku", nullable = false)
    private String JudulBuku;

    @ManyToOne
    @JoinColumn(name = "penulis", insertable = false, updatable = false)
    public Penulis penulis;

    @Column(name = "penulis", nullable = false)
    private long IdPenulis;

    @ManyToOne
    @JoinColumn(name = "penerbit",insertable = false, updatable = false)
    public Penerbit penerbit;

    @Column(name = "penerbit",nullable = false)
    private long IdPenerbit;

    @Column(name = "tahun_terbit",length = 4, nullable = false)
    private String TahunTerbit;

    @Column(name = "stock", nullable = false)
    private int Stock;

    public long getIdBuku() {
        return IdBuku;
    }

    public void setIdBuku(long idBuku) {
        IdBuku = idBuku;
    }

    public String getJudulBuku() {
        return JudulBuku;
    }

    public void setJudulBuku(String judulBuku) {
        JudulBuku = judulBuku;
    }

    public Penulis getPenulis() {
        return penulis;
    }

    public void setPenulis(Penulis penulis) {
        this.penulis = penulis;
    }

    public long getIdPenulis() {
        return IdPenulis;
    }

    public void setIdPenulis(long idPenulis) {
        IdPenulis = idPenulis;
    }

    public Penerbit getPenerbit() {
        return penerbit;
    }

    public void setPenerbit(Penerbit penerbit) {
        this.penerbit = penerbit;
    }

    public long getIdPenerbit() {
        return IdPenerbit;
    }

    public void setIdPenerbit(long idPenerbit) {
        IdPenerbit = idPenerbit;
    }

    public String getTahunTerbit() {
        return TahunTerbit;
    }

    public void setTahunTerbit(String tahunTerbit) {
        TahunTerbit = tahunTerbit;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int stock) {
        Stock = stock;
    }
}