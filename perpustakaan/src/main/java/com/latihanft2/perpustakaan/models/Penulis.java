package com.latihanft2.perpustakaan.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table (name = "penulis")
public class Penulis extends CommonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_penulis", nullable = false)
    private long IdPenulis;

    @Column(name = "nama_penulis",length = 50, nullable = false)
    private String NamaPenulis;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "dob", nullable = false)
    private Date Dob;

    @Column(name = "domisili",length = 30, nullable = false)
    private String Domisili;

    @Column(name = "nomor_telepon",length = 20, nullable = false)
    private String NomorTelepon;

    public long getIdPenulis() {
        return IdPenulis;
    }

    public void setIdPenulis(long idPenulis) {
        IdPenulis = idPenulis;
    }

    public String getNamaPenulis() {
        return NamaPenulis;
    }

    public void setNamaPenulis(String namaPenulis) {
        NamaPenulis = namaPenulis;
    }

    public Date getDob() {
        return Dob;
    }

    public void setDob(Date dob) {
        Dob = dob;
    }

    public String getDomisili() {
        return Domisili;
    }

    public void setDomisili(String domisili) {
        Domisili = domisili;
    }

    public String getNomorTelepon() {
        return NomorTelepon;
    }

    public void setNomorTelepon(String nomorTelepon) {
        NomorTelepon = nomorTelepon;
    }
}
