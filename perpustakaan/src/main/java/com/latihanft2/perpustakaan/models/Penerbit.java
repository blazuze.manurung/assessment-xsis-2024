package com.latihanft2.perpustakaan.models;

import javax.persistence.*;

@Entity
@Table(name = "penerbit")
public class Penerbit extends CommonEntity{

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "id_penerbit", nullable = false)
    private long IdPenerbit;

    @Column(name = "nama_penerbit", length = 50, nullable = false)
    private String NamaPenerbit;

    @Column(name = "tahun_berdiri", length = 4, nullable = false)
    private String TahunBerdiri;

    @Column(name = "asal_penerbit", length = 20, nullable = false)
    private String AsalPenerbit;

    @Column(name = "nomor_telepon", length = 20, nullable = false)
    private String NomorTelepon;

    public long getIdPenerbit() {
        return IdPenerbit;
    }

    public void setIdPenerbit(long idPenerbit) {
        IdPenerbit = idPenerbit;
    }

    public String getNamaPenerbit() {
        return NamaPenerbit;
    }

    public void setNamaPenerbit(String namaPenerbit) {
        NamaPenerbit = namaPenerbit;
    }

    public String getTahunBerdiri() {
        return TahunBerdiri;
    }

    public void setTahunBerdiri(String tahunBerdiri) {
        TahunBerdiri = tahunBerdiri;
    }

    public String getAsalPenerbit() {
        return AsalPenerbit;
    }

    public void setAsalPenerbit(String asalPenerbit) {
        AsalPenerbit = asalPenerbit;
    }

    public String getNomorTelepon() {
        return NomorTelepon;
    }

    public void setNomorTelepon(String nomorTelepon) {
        NomorTelepon = nomorTelepon;
    }
}
