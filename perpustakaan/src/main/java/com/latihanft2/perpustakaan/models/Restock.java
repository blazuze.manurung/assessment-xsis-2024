package com.latihanft2.perpustakaan.models;

import javax.persistence.*;

@Entity
@Table(name = "restock")
public class Restock extends CommonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_restock", nullable = false)
    private long IdRestock;

    @ManyToOne
    @JoinColumn(name = "judul_buku", insertable = false, updatable = false)
    public Buku buku;

    @Column(name = "judul_buku", nullable = false)
    private long IdBuku;

    @Column(name = "jumlah", nullable = false)
    private int Jumlah;

    public long getIdRestock() {
        return IdRestock;
    }

    public void setIdRestock(long idRestock) {
        IdRestock = idRestock;
    }

    public Buku getBuku() {
        return buku;
    }

    public void setBuku(Buku buku) {
        this.buku = buku;
    }

    public long getIdBuku() {
        return IdBuku;
    }

    public void setIdBuku(long idBuku) {
        IdBuku = idBuku;
    }

    public int getJumlah() {
        return Jumlah;
    }

    public void setJumlah(int jumlah) {
        Jumlah = jumlah;
    }
}
