$("#addPenerbitBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addPenerbitBtnCreate").click(function(){
	var namaPenerbit = $("#namaPenerbitInput").val();
	var tahunBerdiri = $("#tahunBerdiriInput").val();
	var asalPenerbit = $("#asalPenerbitInput").val();
	var nomorTelepon = $("#nomorTeleponInput").val();

    if(namaPenerbit == ""){
        $("#errNamaPenerbit").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNamaPenerbit").text("");
    }
    if(tahunBerdiri == ""){
        $("#errTahunBerdiri").text("Tahun Berdiri tidak boleh kosong!");
        return
    }else{
        $("#errTahunBerdiri").text("");
    }
    if(asalPenerbit == ""){
        $("#errAsalPenerbit").text("Asal Penerbit tidak boleh kosong!");
        return
    }else{
        $("#errAsalPenerbit").text("");
    }
    if(nomorTelepon == ""){
        $("#errNomorTelepon").text("Nomor Telepon tidak boleh kosong!");
        return
    }else{
        $("#errNomorTelepon").text("");
    }

	var obj = {};
	obj.namaPenerbit = namaPenerbit;
	obj.tahunBerdiri = tahunBerdiri;
	obj.asalPenerbit = asalPenerbit;
	obj.nomorTelepon = nomorTelepon;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url : "/api/addpenerbit",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
				getAllPenerbit();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})