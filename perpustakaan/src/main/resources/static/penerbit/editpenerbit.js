$(document).ready(function() {
	GetPenerbitById();
})

function GetPenerbitById() {
	var idPenerbit = $("#editPenerbitId").val();
	$.ajax({
		url: "/api/getbyidpenerbit/" + idPenerbit,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#namaPenerbitInput").val(data.namaPenerbit);
			$("#tahunBerdiriInput").val(data.tahunBerdiri);
			$("#asalPenerbitInput").val(data.asalPenerbit);
			$("#nomorTeleponInput").val(data.nomorTelepon);
		}
	})
}

$("#editPenerbitBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editPenerbitBtnCreate").click(function() {
	var idPenerbit= $("#editPenerbitId").val();
    var namaPenerbit = $("#namaPenerbitInput").val();
    var tahunBerdiri = $("#tahunBerdiriInput").val();
    var asalPenerbit = $("#asalPenerbitInput").val();
    var nomorTelepon = $("#nomorTeleponInput").val();

    if(namaPenerbit == ""){
        $("#errNamaPenerbit").text("Nama Penerbit tidak boleh kosong!");
        return;
    }else{
        $("#errNamaPenerbit").text("");
    }
    if(tahunBerdiri == ""){
        $("#errTahunBerdiri").text("Tahun Berdiri tidak boleh kosong");
        return
    }else{
        $("#errTahunBerdiri").text("");
    }
    if(asalPenerbit == ""){
        $("#errAsalPenerbit").text("Asal Penerbit tidak boleh kosong");
        return
    }else{
        $("#errAsalPenerbit").text("");
    }
    if(nomorTelepon == ""){
        $("#errNomorTelepon").text("Nomor Telepon tidak boleh kosong");
        return
    }else{
        $("#errNomorTelepon").text("");
    }

	var obj = {};
	obj.idPenerbit = idPenerbit;
    obj.namaPenerbit = namaPenerbit;
	obj.tahunBerdiri = tahunBerdiri;
	obj.asalPenerbit = asalPenerbit;
	obj.nomorTelepon = nomorTelepon;


	var myJson = JSON.stringify(obj);

//    console.log(myJson);

	$.ajax({
		url: "/api/editpenerbit/" + idPenerbit,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
				getAllPenerbit();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})