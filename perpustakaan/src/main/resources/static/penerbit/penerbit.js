function GetAllPenerbit(){
	$("#penerbitTable").html(
		`<thead>
			<tr>
				<th>ID</th>
				<th>Penerbit</th>
				<th>Tahun Berdiri</th>
				<th>Asal Penerbit</th>
				<th>Nomor Telepon</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="penerbitTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getallpenerbit",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#penerbitTBody").append(
					`
					<tr>
						<td>${data[i].idPenerbit}</td>
						<td>${data[i].namaPenerbit}</td>
						<td>${data[i].tahunBerdiri}</td>
						<td>${data[i].asalPenerbit}</td>
						<td>${data[i].nomorTelepon}</td>
						<td>
							<button value="${data[i].idPenerbit}" onClick="editPenerbit(this.value)" class="btn btn-warning">
								<i class="bi-pencil-square"></i>
							</button>
							<button value="${data[i].idPenerbit}" onClick="deletePenerbit(this.value)" class="btn btn-danger">
								<i class="bi-trash"></i>
							</button>
						</td>
					</tr>
					`
				)
			}
		}
	});
}
function GetPenerbitByPage(currentPage, length) {
console.log("ini length : " + length);
console.log("ini currentPage : " + currentPage);
        $("#penerbitTable").html(
        `
           <thead>
           <tr>
              <th>ID</th>
              <th>Penerbit</th>
              <th>Tahun Berdiri</th>
              <th>Asal Penerbit</th>
              <th>Nomor Telepon</th>
              <th>Action</th>
           </tr>
           </thead>
           <tbody id="penerbitTBody"></tbody>
        `
        );
    $.ajax({
        url : '/api/penerbitmapped?page=' + currentPage + '&size=' + length,
        type : 'GET',
        contentType : 'application/json',
        success : function(data) {

        for (let i = 0; i < data.penerbit.length; i++) {
        $("#penerbitTBody").append(
        `
            <tr>
                <td>${data.penerbit[i].idPenerbit}</td>
                <td>${data.penerbit[i].namaPenerbit}</td>
                <td>${data.penerbit[i].tahunBerdiri}</td>
                <td>${data.penerbit[i].asalPenerbit}</td>
                <td>${data.penerbit[i].nomorTelepon}</td>
                <td>
                    <button value="${data.penerbit[i].idPenerbit}" onClick="editPenerbit(this.value)" class="btn btn-warning">
                        <i class="bi-pencil-square"></i>
                    </button>
                    <button value="${data.penerbit[i].idPenerbit}" onClick="deletePenerbit(this.value)" class="btn btn-danger">
                        <i class="bi-trash"></i>
                    </button>
                </td>
            </tr>
        `);
        }
         $("#pagination").html(
         `
            <nav aria-label="Page navigation">
            <ul class="pagination">
            <li class="page-item"><a class="page-link" onclick="GetPenerbitByPage(data.currentPage - 1 ,length)">Previous</a></li>
            let index = 1
            for (let i = 0; i < data.totalPages; i++) {
                <li class="page-item"><a id="pageslink" class="page-link" onclick="GetPenerbitByPage(i, length )">' + index + '</a></li>;
                index++;
            }
            <li class="page-item"><a class="page-link" onclick="GetPenerbitByPage((data.currentPage + 1), length)">Next</a></li>
            </ul>
            </nav>
         `);
        }
    });
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
function PenerbitList(currentPage, length) {
	$.ajax({
		url : '/api/penerbitmapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = "<table class='table table-stripped mt-3'>";
			table += "<tr> <th width='10%' class='text-center'>Nomor<th>ID</th> <th>Penerbit</th> <th>Tahun Berdiri</th> <th>Asal Penerbit</th> <th>Nomor Telepon</th> <th>Action</th>"
			for (let i = 0; i < data.penerbit.length; i++) {
				table += "<tr>";
				table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
				table += "<td>" + data.penerbit[i].idPenerbit + "</td>";
				table += "<td>" + data.penerbit[i].namaPenerbit + "</td>";
				table += "<td>" + data.penerbit[i].tahunBerdiri + "</td>";
				table += "<td>" + data.penerbit[i].asalPenerbit + "</td>";
				table += "<td>" + data.penerbit[i].nomorTelepon + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.penerbit[i].idPenerbit + "' onclick=editPenerbit(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data.penerbit[i].idPenerbit + "' onclick=deletePenerbit(this.value)>Delete</button></td>";

				// table += "<td><input type='checkbox' onclick='SelectedItem()' class='mychecked' id='listdelete' value='"+data.category[i].id+"'></td>"
				table += "</tr>";
			}
			table += "</table>";
			table += '<select class="custom-select" id="size" onchange="PenerbitList(0,this.value)">'
            table += '<option value="3" selected>..</option>'
            table += '<option value="5">5</option>'
            table += '<option value="10">10</option>'
            table += '<option value="15">15</option>'
            table += '</select>';
			table += "<br>"
			table += '<nav aria-label="Page navigation">';
			table += '<ul class="pagination mt-2">'
			table += '<li class="page-item"><a class="page-link" onclick="PenerbitList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
			let index = 1;
			for (let i = 0; i < data.totalPages; i++) {
				table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="PenerbitList(' + i + ',' + length + ')">' + index + '</a></li>'
				index++;
			}
			table += '<li class="page-item"><a class="page-link" onclick="PenerbitList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#penerbitList').html(table);
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/penerbit/addpenerbit",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Penerbit");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editPenerbit(idPenerbit){
	$.ajax({
		url: "/penerbit/editpenerbit/" + idPenerbit,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Penerbit");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deletePenerbit(idPenerbit){
	$.ajax({
		url: "/penerbit/deletepenerbit/" + idPenerbit,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Penerbit");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

//function SearchPenerbit(request) {
////console.log(request)
//    if (request.length > 0)
//    {
//        $.ajax({
//            url: '/api/searchpenerbit/' + request,
//            type: 'GET',
//            contentType: 'application/json',
//            success: function (result) {
//                //console.log(result)
//                if (result.length > 0)
//                {
//                    for (i = 0; i < result.length; i++) {
//                    $("#penerbitTBody").html(
//                        `
//                        <tr>
//                            <td>${result[i].idPenerbit}</td>
//                            <td>${result[i].namaPenerbit}</td>
//                            <td>${result[i].tahunBerdiri}</td>
//                            <td>${result[i].asalPenerbit}</td>
//                            <td>${result[i].nomorTelepon}</td>
//                            <td>
//                                <button value="${result[i].idPenerbit}" onClick="editPenerbit(this.value)" class="btn btn-warning">
//                                    <i class="bi-pencil-square"></i>
//                                </button>
//                                <button value="${result[i].idPenerbit}" onClick="deletePenerbit(this.value)" class="btn btn-danger">
//                                    <i class="bi-trash"></i>
//                                </button>
//                            </td>
//                        </tr>
//                        `)
//                    }
//                }
//                else {
//                     $("#penerbitTBody").html(
//                     `
//                        <tr>
//                        <td colspan='6' class='text-center'>No data</td>
//                        </tr>
//                     `)
//                }
//            }
//        });
//    }
//    else {
//        GetPenerbitByPage(0, 5);
//    }
//}

function SearchPenerbit(request) {
	//console.log(request)
	if (request.length > 0)
	{
		$.ajax({
			url: '/api/searchpenerbit/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
				//console.log(result)
				let table = "<table class='table table-stripped mt-3'>";
			    table += "<tr> <th width='10%' class='text-center'>Nomor<th>ID</th> <th>Penerbit</th> <th>Tahun Berdiri</th> <th>Asal Penerbit</th> <th>Nomor Telepon</th> <th>Action</th>"
				if (result.length > 0)
				{
					for (let i = 0; i < result.length; i++) {
						table += "<tr>";
						table += "<td class='text-center'>" + (i+1) + "</td>";
                        table += "<td>" + result[i].idPenerbit + "</td>";
                        table += "<td>" + result[i].namaPenerbit + "</td>";
                        table += "<td>" + result[i].tahunBerdiri + "</td>";
                        table += "<td>" + result[i].asalPenerbit + "</td>";
                        table += "<td>" + result[i].nomorTelepon + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].idPenerbit + "' onclick=editPenerbit(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + result[i].id + "' onclick=deletePenerbit(this.value)>Delete</button></td>";
						table += "</tr>";
					}
				} else {
					table += "<tr>";
					table += "<td colspan='6' class='text-center'>No data</td>";
					table += "</tr>";
				}
				table += "</table>";
				$('#penerbitList').html(table);
			}
		});
	} else {
		PenerbitList(0,5);
	}
}

$(document).ready(function(){
//	GetPenerbitByPage(0, 5);
PenerbitList(0,5);
})