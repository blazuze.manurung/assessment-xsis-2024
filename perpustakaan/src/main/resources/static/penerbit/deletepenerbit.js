$(document).ready(function() {
	GetPenerbitById();

})

function GetPenerbitById() {
	var idPenerbit = $("#deletePenerbitId").val();
	$.ajax({
		url: "/api/getbyidpenerbit/" + idPenerbit,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#namaPenerbitDel").text(data.namaPenerbit);
		}
	})
}

$("#deletePenerbitBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deletePenerbitBtnDelete").click(function() {
	var idPenerbit = $("#deletePenerbitId").val();
	$.ajax({
		url : "/api/deletepenerbit/" + idPenerbit,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            location.reload();
            getAllPenerbit();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})