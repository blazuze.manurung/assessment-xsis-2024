function PenulisList(currentPage, length) {
	$.ajax({
		url : '/api/penulismapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = "<table class='table table-stripped mt-3'>";
			table += "<tr> <th width='10%' class='text-center'>Nomor<th>ID</th> <th>Penulis</th> <th>Tanggal Lahir</th> <th>Domisili</th> <th>Nomor Telepon</th> <th>Action</th>"
			for (let i = 0; i < data.penulis.length; i++) {
			    var dateString = data.penulis[i].dob
			    console.log(dateString);
                var date = new Date(dateString);
                console.log(date);
                var year = date.getFullYear();
                var month = ('0' + (date.getMonth() + 1)).slice(-2);
                var day = ('0' + date.getDate()).slice(-2);
                var formattedDate = year + '-' + month + '-' + day;
                console.log(formattedDate);
				table += "<tr>";
				table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
				table += "<td>" + data.penulis[i].idPenulis + "</td>";
				table += "<td>" + data.penulis[i].namaPenulis + "</td>";
				table += "<td>" + formattedDate + "</td>";
				table += "<td>" + data.penulis[i].domisili + "</td>";
				table += "<td>" + data.penulis[i].nomorTelepon + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.penulis[i].idPenulis + "' onclick=editPenulis(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data.penulis[i].idPenulis + "' onclick=deletePenulis(this.value)>Delete</button></td>";

				// table += "<td><input type='checkbox' onclick='SelectedItem()' class='mychecked' id='listdelete' value='"+data.category[i].id+"'></td>"
				table += "</tr>";
			}
			table += "</table>";
			table += '<select class="custom-select" id="size" onchange="PenulisList(0,this.value)">'
            table += '<option value="3" selected>..</option>'
            table += '<option value="5">5</option>'
            table += '<option value="10">10</option>'
            table += '<option value="15">15</option>'
            table += '</select>';
			table += "<br>"
			table += '<nav aria-label="Page navigation">';
			table += '<ul class="pagination mt-2">'
			table += '<li class="page-item"><a class="page-link" onclick="PenulisList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
			let index = 1;
			for (let i = 0; i < data.totalPages; i++) {
				table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="PenulisList(' + i + ',' + length + ')">' + index + '</a></li>'
				index++;
			}
			table += '<li class="page-item"><a class="page-link" onclick="PenulisList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#penulisList').html(table);
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/penulis/addpenulis",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Penulis");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editPenulis(idPenulis){
	$.ajax({
		url: "/penulis/editpenulis/" + idPenulis,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Penulis");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deletePenulis(idPenulis){
	$.ajax({
		url: "/penulis/deletepenulis/" + idPenulis,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Penulis");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function SearchPenulis(request) {
	//console.log(request)
	if (request.length > 0)
	{
		$.ajax({
			url: '/api/searchpenulis/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
				//console.log(result)
				let table = "<table class='table table-stripped mt-3'>";
			    table += "<tr> <th width='10%' class='text-center'>Nomor<th>ID</th> <th>Penulis</th> <th>Tanggal Lahir</th> <th>Domisili</th> <th>Nomor Telepon</th> <th>Action</th>"
				if (result.length > 0)
				{
					for (let i = 0; i < result.length; i++) {
						table += "<tr>";
						table += "<td class='text-center'>" + (i+1) + "</td>";
                        table += "<td>" + result[i].idPenulis + "</td>";
                        table += "<td>" + result[i].namaPenulis + "</td>";
                        var dateString = result[i].dob
                        var date = new Date(dateString);
                        var year = date.getFullYear();
                        var month = ('0' + (date.getMonth() + 1)).slice(-2);
                        var day = ('0' + date.getDate()).slice(-2);
                        var formattedDate = year + '-' + month + '-' + day;
                        table += "<td>" + formattedDate + "</td>";
                        table += "<td>" + result[i].domisili + "</td>";
                        table += "<td>" + result[i].nomorTelepon + "</td>";
				        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].idPenulis + "' onclick=editPenulis(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + result[i].idPenulis + "' onclick=deletePenulis(this.value)>Delete</button></td>";
						table += "</tr>";
					}
				} else {
					table += "<tr>";
					table += "<td colspan='6' class='text-center'>No data</td>";
					table += "</tr>";
				}
				table += "</table>";
				$('#penulisList').html(table);
			}
		});
	} else {
		PenulisList(0,5);
	}
}

$(document).ready(function(){
PenulisList(0,5);
})