$(document).ready(function() {
	GetPenulisById();
})

function GetPenulisById() {
	var idPenulis = $("#editPenulisId").val();
	$.ajax({
		url: "/api/getbyidpenulis/" + idPenulis,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#namaPenulisInput").val(data.namaPenulis);
			$("#dobPenulis").val(data.dob);
			$("#domisiliPenulisInput").val(data.domisili);
			$("#nomorTeleponInput").val(data.nomorTelepon);
		}
	})
}

$("#editPenulisBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editPenulisBtnCreate").click(function() {
	var idPenulis= $("#editPenulisId").val();
    var namaPenulis = $("#namaPenulisInput").val();
    var dobPenulis = $("#dobPenulis").val();
    var domisiliPenulis = $("#domisiliPenulisInput").val();
    var nomorTelepon = $("#nomorTeleponInput").val();

    if(namaPenulis == ""){
        $("#errNamaPenulis").text("Nama Penulis tidak boleh kosong!");
        return;
    }else{
        $("#errNamaPenulis").text("");
    }
    if(dobPenulis == ""){
        $("#errDobPenulis").text("Tanggal Lahir tidak boleh kosong");
        return
    }else{
        $("#errDobPenulis").text("");
    }
    if(domisiliPenulis == ""){
        $("#errDomisiliPenulis").text("Domisili Penulis tidak boleh kosong");
        return
    }else{
        $("#errDomisiliPenulis").text("");
    }
    if(nomorTelepon == ""){
        $("#errNomorTelepon").text("Nomor Telepon tidak boleh kosong");
        return
    }else{
        $("#errNomorTelepon").text("");
    }

	var obj = {};
	obj.idPenulis = idPenulis;
    obj.namaPenulis = namaPenulis;
	obj.dob = dobPenulis;
	obj.domisili = domisiliPenulis;
	obj.nomorTelepon = nomorTelepon;


	var myJson = JSON.stringify(obj);

//    console.log(myJson);

	$.ajax({
		url: "/api/editpenulis/" + idPenulis,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
				getAllPenulis();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})