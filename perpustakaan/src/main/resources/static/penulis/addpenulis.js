$("#addPenulisBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addPenulisBtnCreate").click(function(){
	var namaPenulis  = $("#namaPenulisInput").val();
	var dobPenulis = $("#dobPenulis").val();
	var domisiliPenulis = $("#domisiliPenulisInput").val();
	var nomorTelepon = $("#nomorTeleponInput").val();

    if(namaPenulis == ""){
        $("#errNamaPenulis").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNamaPenulis").text("");
    }
    if(dobPenulis == ""){
        $("#errDobPenulis").text("Tanggal Lahir tidak boleh kosong!");
        return
    }else{
        $("#errDobPenulis").text("");
    }
    if(domisiliPenulis == ""){
        $("#errDomisiliPenulis").text("Domisili tidak boleh kosong!");
        return
    }else{
        $("#errDomisiliPenulis").text("");
    }
    if(nomorTelepon == ""){
        $("#errNomorTelepon").text("Nomor Telepon tidak boleh kosong!");
        return
    }else{
        $("#errNomorTelepon").text("");
    }

	var obj = {};
	obj.namaPenulis = namaPenulis;
	obj.dob = dobPenulis;
	obj.domisili = domisiliPenulis;
	obj.nomorTelepon = nomorTelepon;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url : "/api/addpenulis",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
				getAllPenulis();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})