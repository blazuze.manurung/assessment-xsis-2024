$(document).ready(function() {
	GetPenulisById();

})

function GetPenulisById() {
	var idPenulis = $("#deletePenulisId").val();
	$.ajax({
		url: "/api/getbyidpenulis/" + idPenulis,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#namaPenulisDel").text(data.namaPenulis);
		}
	})
}

$("#deletePenulisBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deletePenulisBtnDelete").click(function() {
	var idPenulis = $("#deletePenulisId").val();
	$.ajax({
		url : "/api/deletepenulis/" + idPenulis,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            location.reload();
            getAllPenulis();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})