$("#addBukuBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$(document).ready(function(){
        	opsiReload();
})

function opsiReload(){
        var penulisSel = document.getElementById("opsiPenulis");
        var penerbitSel = document.getElementById("opsiPenerbit");

        $.ajax({
            url : "/api/getallpenulis",
            type : "GET",
            contentType : "application/json",
            success: function(data){
                for(i = 0; i<data.length; i++){
                    penulisSel.options[penulisSel.options.length] = new Option(data[i].namaPenulis, data[i].idPenulis);
                }
            }
        });

        $.ajax({
            url : "/api/getallpenerbit",
            type : "GET",
            contentType : "application/json",
            success: function(data){
                for(i = 0; i<data.length; i++){
                    penerbitSel.options[penerbitSel.options.length] = new Option(data[i].namaPenerbit, data[i].idPenerbit);
                }
            }
        });
}

$("#addBukuBtnCreate").click(function(){
	var judulBuku = $("#nameInput").val();
	var namaPenulis = $("#opsiPenulis").val();
	var namaPenerbit = $("#opsiPenerbit").val();
	var tahunTerbit = $("#tahunInput").val();
	var stock = $("#stockInput").val();

	if(judulBuku == ""){
		$("#errName").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	if(namaPenulis == ""){
    	$("#errOpsiPenulis").text("Penulis tidak boleh kosong!");
    	return;
    } else {
    	$("#errOpsiPenulis").text("");
    }

    if(namaPenerbit == ""){
    	$("#errOpsiPenerbit").text("Penerbit tidak boleh kosong!");
    	return;
    } else {
    	$("#errOpsiPenerbit").text("");
    }

    if(tahunTerbit == ""){
        $("#errTahun").text("Tahun tidak boleh kosong!");
        return;
    } else {
        $("#errTahun").text("");
    }

    if(stock == ""){
        $("#errStock").text("Stock tidak boleh kosong!");
        return;
    } else {
        $("#errStock").text("");
    }


	var obj = {};
	obj.judulBuku = judulBuku;
	obj.idPenulis = namaPenulis;
	obj.idPenerbit = namaPenerbit;
	obj.tahunTerbit = tahunTerbit;
	obj.stock = stock;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url : "/api/addbuku",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){
			$(".modal").modal("hide")
			location.reload();
			GetAllBuku();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})