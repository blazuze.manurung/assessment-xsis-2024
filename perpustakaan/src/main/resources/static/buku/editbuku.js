$(document).ready(function() {
	GetBukuById();
	opsiReload();
})

function opsiReload() {
    var penulisSel = document.getElementById("editOpsiPenulis");
    var penerbitSel = document.getElementById("editOpsiPenerbit");

    $.ajax({
        url : "/api/getallpenulis",
        type : "GET",
        contentType : "application/json",
        async : false,
        success: function(data){
            for(i = 0; i<data.length; i++){
                penulisSel.options[penulisSel.options.length] = new Option(data[i].namaPenulis, data[i].idPenulis);
            }
        }
    });
    $.ajax({
        url : "/api/getallpenerbit",
        type : "GET",
        contentType : "application/json",
        async : false,
        success: function(data){
            for(i = 0; i<data.length; i++){
                penerbitSel.options[penerbitSel.options.length] = new Option(data[i].namaPenerbit, data[i].idPenerbit);
            }
        }
    });
}

function GetBukuById() {
	var idBuku = $("#EditBukuId").val();
	$.ajax({
		url: "/api/getbyidbuku/" + idBuku,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameInput").val(data.judulBuku);
			$("#editOpsiPenulis").val(data.idPenulis);
			$("#editOpsiPenerbit").val(data.idPenerbit);
            $("#tahunInput").val(data.tahunTerbit);
            $("#stockInput").val(data.stock);
		}
	})
}

$("#editBukuBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editBukuBtnCreate").click(function() {
    var idBuku = $("#EditBukuId").val();
	var judulBuku = $("#nameInput").val();
    var idPenulis = $("#editOpsiPenulis").val();
    var idPenerbit = $("#editOpsiPenerbit").val();
    var tahunTerbit = $("#tahunInput").val();
    var stock = $("#stockInput").val();

	if (judulBuku == "") {
		$("#errName").text("Judul tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	if (idPenulis == "") {
    	$("#errOpsiPenulis").text("Penulis tidak boleh kosong!");
    	return;
    } else {
    	$("#errOpsiPenulis").text("");
    }

    if (idPenerbit == "") {
    	$("#errOpsiPenerbit").text("Penerbit tidak boleh kosong!");
    	return;
    } else {
    	$("#errOpsiPenerbit").text("");
    }

    if (tahunTerbit == "") {
        $("#errTahun").text("Tahun tidak boleh kosong!");
        return;
    } else {
        $("#errTahun").text("");
    }

    if (stock == "") {
        $("#errStock").text("Stock tidak boleh kosong!");
        return;
    } else {
        $("#errStock").text("");
    }


	var obj = {};
	obj.idBuku = idBuku;
	obj.judulBuku = judulBuku;
    obj.idPenulis = idPenulis;
    obj.idPenerbit = idPenerbit;
    obj.tahunTerbit = tahunTerbit;
    obj.stock = stock;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editbuku/" + idBuku,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
				getAllBuku();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})