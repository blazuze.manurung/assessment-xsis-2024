$(document).ready(function() {
	GetBukuById();

})

function GetBukuById() {
	var idBuku = $("#deleteBukuId").val();
	$.ajax({
		url: "/api/getbyidbuku/" + idBuku,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#namaBukuDel").text(data.judulBuku);
		}
	})
}

$("#deleteBukuBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteBukuBtnDelete").click(function() {
	var idBuku = $("#deleteBukuId").val();
	$.ajax({
		url : "/api/deletebuku/" + idBuku,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            location.reload();
            getAllBuku();

		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})