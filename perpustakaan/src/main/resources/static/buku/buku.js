function BukuList(currentPage, length) {
	$.ajax({
		url : '/api/bukumapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

		let table = "<table class='table table-stripped mt-3'>";
		table += "<tr> <th width='10%' class='text-center'>Nomor</th> <th>Kode Buku</th> <th>Judul Buku</th> <th>Penulis</th> <th>Penerbit</th> <th>Tahun Terbit</th> <th>Stok</th> <th>Action</th>"
		for (let i = 0; i < data.buku.length; i++) {
		    //const formattedDate = formatDate(data.penulis[i].dob);
			table += "<tr>";
			table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
			table += "<td>" + data.buku[i].idBuku + "</td>";
			table += "<td>" + data.buku[i].judulBuku + "</td>";
			table += "<td>" + ((data.buku[i].penulis).namaPenulis) + "</td>";
			table += "<td>" + ((data.buku[i].penerbit).namaPenerbit) + "</td>";
			table += "<td>" + data.buku[i].tahunTerbit + "</td>";
			table += "<td>" + data.buku[i].stock + "</td>";
			table += "<td><button class='btn btn-primary btn-sm' value='" + data.buku[i].idBuku + "' onclick=editBuku(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data.buku[i].idBuku + "' onclick=deleteBuku(this.value)>Delete</button></td>";
			// table += "<td><input type='checkbox' onclick='SelectedItem()' class='mychecked' id='listdelete' value='"+data.category[i].id+"'></td>"
			table += "</tr>";
		}
		table += "</table>";
	    table += '<select class="custom-select" id="size" onchange="BukuList(0,this.value)">'
        table += '<option value="3" selected>..</option>'
        table += '<option value="5">5</option>'
        table += '<option value="10">10</option>'
        table += '<option value="15">15</option>'
        table += '</select>';
		table += "<br>"
		table += '<nav aria-label="Page navigation">';
		table += '<ul class="pagination">'
		table += '<li class="page-item"><a class="page-link" onclick="BukuList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
		let index = 1;
		for (let i = 0; i < data.totalPages; i++) {
			table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="BukuList(' + i + ',' + length + ')">' + index + '</a></li>'
			index++;
	    }
		table += '<li class="page-item"><a class="page-link" onclick="BukuList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
		table += '</ul>'
		table += '</nav>';
		$('#bukuList').html(table);
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/buku/addbuku",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Buku");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editBuku(idBuku){
	$.ajax({
		url: "/buku/editbuku/" + idBuku,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Buku");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteBuku(idBuku){
	$.ajax({
		url: "/buku/deletebuku/" + idBuku,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Buku");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function SearchBuku(request) {
    //console.log(request)

    if (request.length > 0)
    {
    	$.ajax({
    		url: '/api/searchbuku/' + request,
    		type: 'GET',
    		contentType: 'application/json',
    		success: function (result) {
    			//console.log(result)
				let table = "<table class='table table-stripped mt-3'>";
    		    table += "<tr> <th width='10%' class='text-center'>Nomor</th> <th>Kode Buku</th> <th>Judul Buku</th> <th>Penulis</th> <th>Penerbit</th> <th>Tahun Terbit</th> <th>Stok</th> <th>Action</th>"
    			if (result.length > 0)
    			{
    				for (let i = 0; i < result.length; i++) {
    					table += "<tr>";
    					table += "<td class='text-center'>" + (i+1) + "</td>";
    					table += "<td>" + result[i].idBuku + "</td>";
                        table += "<td>" + result[i].judulBuku + "</td>";
                        table += "<td>" + ((result[i].penulis).namaPenulis) +"</td>";
                        table += "<td>" + ((result[i].penerbit).namaPenerbit) + "</td>";
                        table += "<td>" + result[i].tahunTerbit + "</td>";
                        table += "<td>" + result[i].stock + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].idBuku + "' onclick=editBuku(this.value) >Edit</button> <button class='btn btn-danger btn-sm' value='" + result[i].idBuku + "' onclick=deleteBuku(this.value) >Delete</button></td>";
    			        table += "</tr>";
    				}
    			} else {
    				table += "<tr>";
    				table += "<td colspan='8' class='text-center'>No data</td>";
    				table += "</tr>";
    			}
    			table += "</table>";
    			$('#bukuList').html(table);
    		}
    	});
    } else {
    	BukuList(0,5);
    }
}

$(document).ready(function(){
//	GetAllPenerbit();
BukuList(0,5);
})