package Logic;

import java.util.Scanner;

public class Soal05 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("5. Bilangan Fibonacci");
        System.out.print("Ketikkan Jumlah Angka yang Diinginkan : ");
        int n = input.nextInt();

        int helper1 = 0;
        int helper2 = 1;

        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {

            if (i == 0) {
                hasil[i] = helper2;
            }
            else{
                hasil[i] = helper1 + helper2;
                helper1 = helper2;
                helper2 = hasil[i];
            }
        }

        System.out.print("Bilangan Fibonacci : ");

        for (int i = 0; i < hasil.length; i++) {
            if (i == hasil.length - 1)
            {
                System.out.print(hasil[i]);
            }
            else {
                System.out.print(hasil[i] + ", ");
            }
        }
    }
}
