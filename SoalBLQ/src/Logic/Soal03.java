package Logic;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Soal03 {

    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("3. Kalkulasi tarif parkir");
        System.out.println("   8 Jam pertama : 1000/jam");
        System.out.println("   Lebih dari 8 Jam s.d. 24 Jam : 8000 flat");
        System.out.println("   Lebih dari 24 Jam : 15000 + 1000 / 1 Jam");
        System.out.println("Contoh Input Tanggal : 28 Januari 2020 | 07:30:34");
        System.out.println("Masukkan Tanggal Masuk : ");
        String masuk = input.nextLine();

        System.out.println("Masukkan Tanggal Keluar : ");
        String keluar = input.nextLine();

        Date masukDate = null;
        Date keluarDate = null;
        boolean flagMasuk = true, flagKeluar = true;

        DateFormat df = new SimpleDateFormat("dd MMMM yyyy | HH:mm:ss", new Locale("ID"));
        while (flagMasuk || flagKeluar){
            try {
                masukDate = df.parse(masuk);
                flagMasuk = false;
            } catch (ParseException e) {
                System.out.println("Waktu Masuk yang Diinputkan Salah");
                System.out.println("Masukkan Tanggal Masuk : ");
                masuk = input.nextLine();
            }

            try {
                keluarDate = df.parse(keluar);
                flagKeluar = false;
            } catch (ParseException e) {
                System.out.println("Waktu Keluar yang Diinputkan Salah");
                System.out.println("Masukkan Tanggal Keluar : ");
                keluar = input.nextLine();
            }
        }


//         dalam miliseconds
        long bedaWaktu = keluarDate.getTime() - masukDate.getTime();

//         milisecond -> second (/1000)
//         second -> minute (/60)
//         minute -> hour (/60)
        long detikBeda = bedaWaktu / (1000);
        int hargaParkir = 0;
        int berapaHari = 0;
        int tampilanJam = (int) (detikBeda / 3600);
        int tampilanMenit = (int) (detikBeda / 60) - (tampilanJam * 60);
        int tampilanDetik = (int) detikBeda - ((tampilanMenit * 60) + (tampilanJam * 3600));

        System.out.println("Lama Anda Parkir : " + tampilanJam + " Jam " + tampilanMenit + " Menit " + tampilanDetik + " Detik");

        if(detikBeda <= 8 * 3600)
        {
            hargaParkir = (int) (detikBeda / 60 / 60) * 1000;
            System.out.println("Tarif Parkir : " + hargaParkir);
        }

        else if (detikBeda > 8 * 3600 && detikBeda <= 24 * 3600)
        {
            hargaParkir = 8000;
            System.out.println("Tarif Parkir : " + hargaParkir);
        }
        else
        {
            berapaHari = (int) (detikBeda / 3600) / 24;
            hargaParkir = (int) ((berapaHari * 15000) + (((detikBeda / 3600) - (berapaHari * 24)) * 1000));
            System.out.println("Tarif Parkir : " + hargaParkir);
        }
    }
}
