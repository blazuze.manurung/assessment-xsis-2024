package Logic;

import java.util.Scanner;

public class Soal12 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("12. Mengurutkan tanpa fungsi sort");
        System.out.print("Tolong Masukkan Deret Angka : ");
        String text = input.nextLine().trim();

        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray [i] = Integer.parseInt(textArray[i]);
        }

        int hitung = 0;
        int[] hitungUrutan = new int[intArray.length];

        for (int i = 0; i < intArray.length; i++) {
            for (int j = 0; j < intArray.length; j++) {
                if (intArray[i] > intArray[j]){
                    hitung++;
                }
            }
            hitungUrutan[i] = hitung;
            hitung = 0;
        }

        for (int i = 0; i < intArray.length; i++) {
            for (int j = 0; j < intArray.length; j++) {
                if (i == hitungUrutan[j]){
                    System.out.print(intArray[j] + " ");
                }
            }
        }
    }
}
