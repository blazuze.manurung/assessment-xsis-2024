package Logic;

import java.util.Scanner;

public class Soal04 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int n = 0 ;

        System.out.println("4. Bilangan Prima");
        System.out.print("Ketikkan Jumlah Angka yang Diinginkan : ");
        n = input.nextInt();

        int hitung = 0;
        int jumlahData = 0;

        int[] hasil = new int[n];

        for (int i = 0; i < n*10; i++) {
            for (int j = 1; j <= i; j++) {
                if (i % j == 0){
                    hitung++;
                }

            }
            if (hitung == 2){
                hasil[jumlahData] = i;
                jumlahData++;
            }
            if (jumlahData == n){
                break;
            }
            hitung = 0;
        }

        System.out.print("Bilangan prima : ");

        for (int i = 0; i < hasil.length; i++) {
            if (i == hasil.length - 1)
            {
                System.out.print(hasil[i]);
            }
            else {
                System.out.print(hasil[i] + ", ");
            }
        }
    }
}
