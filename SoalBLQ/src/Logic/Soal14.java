package Logic;

import java.util.Scanner;

public class Soal14 {

    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("14. Deret : 3 9 0 7 1 2 4");
        System.out.println("   N = 3 -> 7 1 2 4 3 9 0");
        System.out.println("   N = 1 -> 9 0 7 1 2 4 1");
        System.out.print("Tolong Masukkan Deret Angka : ");
        String text = input.nextLine();

        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];
        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);
        }

        System.out.print("Ketikkan Jumlah N yang Diinginkan : ");
        int inputRotasi = input.nextInt();

        int[] rotasi = new int[intArray.length];
        int ambilAwal = 0;

        for (int i = 0; i < inputRotasi; i++) {
            for (int j = 0; j < intArray.length; j++) {
                ambilAwal = intArray[0];
                if (j == intArray.length - 1) {
                    rotasi[j] = ambilAwal;
                } else {
                    rotasi[j] = intArray[j+1];
                }
            }
            for (int j = 0; j < rotasi.length; j++) {
                intArray[j] = rotasi[j];
            }
        }

        System.out.print("N = "+ inputRotasi +" -> ");
        for (int i = 0; i < intArray.length; i++) {
            System.out.print(intArray[i] + " ");
        }
    }
}
