package Logic;

import java.util.Arrays;
import java.util.Scanner;

public class Soal01 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("1. Andi belanja");

        int uangAwal = 1000;
        int[] hargaKacamata = {500, 600, 700, 800};
        int[] hargaBaju = {200,  400, 350};
        int[] hargaSepatu = {400, 350, 200, 300};
        int[] hargaBuku = {100, 50, 150};

        Arrays.sort(hargaKacamata);
        Arrays.sort(hargaBuku);
        Arrays.sort(hargaBaju);
        Arrays.sort(hargaSepatu);

    }
}
