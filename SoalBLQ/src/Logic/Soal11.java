package Logic;

import java.util.Scanner;

public class Soal11 {

    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("11. Input : Afrika");
        System.out.println("    Output :");
        System.out.println("    ***a***");
        System.out.println("    ***k***");
        System.out.println("    ***i***");
        System.out.println("    ***r***");
        System.out.println("    ***f***");
        System.out.println("    ***A***");
        System.out.print("Masukkan Data : ");

        String text = input.nextLine();

        StringBuilder textReverse = new StringBuilder();
        textReverse.append(text);
        textReverse.reverse();
        String textReverseString = textReverse.toString();
        char[] textReverseCharArray = textReverseString.toCharArray();

        if(textReverseCharArray.length % 2 == 0){
            System.out.println("Output : ");
            for (int i = 0; i < textReverseCharArray.length; i++) {
                System.out.println("***" + textReverseCharArray[i] + "***");
            }
        }

        else {
            System.out.println("Output : ");
            for (int i = 0; i < textReverseCharArray.length; i++) {
                System.out.println("**" + textReverseCharArray[i] + "**");
            }
        }
    }
}
