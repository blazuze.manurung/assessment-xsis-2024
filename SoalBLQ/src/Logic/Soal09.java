package Logic;

import java.util.Scanner;

public class Soal09 {

    public static void Resolve(){

        Scanner input = new Scanner(System.in);
        int n = 0 ;

        System.out.println("9. N = 3 -> 3 6 9");
        System.out.println("   N = 4 -> 4 8 12 16");
        System.out.println("   N = 5 -> 5 10 15 20 25");
        System.out.print("Ketikkan Jumlah N yang Diinginkan : ");
        n = input.nextInt();

        int helper = n;
        int[] hasil = new int[n];

        System.out.print("N = " + n + " -> ");

        for (int i = 0; i < n; i++) {
            if (i == 0){
                System.out.print(helper + " ");
            }
            else
            {
                helper += n;
                System.out.print(helper + " ");
            }
        }
    }
}
