package Logic;

import java.util.Scanner;

public class Soal19 {

    private static String alphabet = "qwertyuiopasdfghjklzxcvbnm";

    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        char[] charAlphabetArray = alphabet.toCharArray();

        System.out.println("19. Pangrams");
        System.out.print("Masukkan text : ");
        String text = input.nextLine().toLowerCase();

        int hitung = 0;

        for (int i = 0; i < charAlphabetArray.length; i++) {
            if (text.contains(alphabet.substring(i, i + 1)))
            {
                hitung++;
            }
        }

        if(hitung == 26)
        {
            System.out.println("Pangrams");
        }
        else
        {
            System.out.println("Bukan Pangrams");
        }

    }
}
