package Logic;

import java.util.Scanner;

public class Soal16 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("16. Teman tidak bisa makan ikan");
        System.out.println("* Asumsi harga pertama adalah makanan mengandung ikan");;
        System.out.println("Contoh input : 42, 50, 30, 70");
        System.out.println("Masukkan Data Harga dalam Ribuan Rupiah : ");
        String data = input.nextLine();

        String[] dataString = data.split(", ");
        double[] dataArrayDou = new double[dataString.length];
        double temenGabisaIkan = 0;
        double temenLain = 0;

        for (int i = 0; i < dataString.length; i++) {
            dataArrayDou[i] = Double.parseDouble(dataString[i]);
            dataArrayDou[i] = dataArrayDou[i] * 115 / 100;
        }

        for (int i = 0; i < dataArrayDou.length; i++) {
            if (i == 0)
            {
                temenLain = temenLain + (dataArrayDou[i] / 3);
            }
            else
            {
                temenLain = temenLain + (dataArrayDou[i] / 4);
                temenGabisaIkan = temenGabisaIkan + (dataArrayDou[i] / 4);
            }
        }

        System.out.println("Temen Ga Bisa Makan Ikan Bayar = Rp." + temenGabisaIkan);
        System.out.println("Yang Bisa Makan Ikan Bayar = Rp." + temenLain);
    }
}
