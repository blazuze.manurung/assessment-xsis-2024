package Logic;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean flag = true;

        while (flag){

            int pilihan = 0;

            System.out.print("Pilih Nomor Soal (1-22) : ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 22)
            {
                System.out.println("Nomor Soal Tidak Tersedia");
                pilihan = input.nextInt();
            }

            switch (pilihan)
            {
                case 1: Soal01.Resolve();
                    break;
                case 2: Soal02.Resolve();
                    break;
                case 3: Soal03.Resolve();
                    break;
                case 4: Soal04.Resolve();
                    break;
                case 5: Soal05.Resolve();
                    break;
                case 6: Soal06.Resolve();
                    break;
                case 7: Soal07.Resolve();
                    break;
                case 8: Soal08.Resolve();
                    break;
                case 9: Soal09.Resolve();
                    break;
                case 10: Soal10.Resolve();
                    break;
                case 11: Soal11.Resolve();
                    break;
                case 12: Soal12.Resolve();
                    break;
                case 13: Soal13.Resolve();
                    break;
                case 14: Soal14.Resolve();
                    break;
                case 15: Soal15.Resolve();
                    break;
                case 16: Soal16.Resolve();
                    break;
                case 17: Soal17.Resolve();
                    break;
                case 18: Soal18.Resolve();
                    break;
                case 19: Soal19.Resolve();
                    break;
                case 20: Soal20.Resolve();
                    break;
                case 21: Soal21.Resolve();
                    break;
                case 22: Soal22.Resolve();
                    break;
            }
            System.out.println();
            System.out.print("Pilih Soal Lagi? (y/n) : ");
            input.nextLine(); //skip bugged
            String answer = input.nextLine();

            if (!answer.equalsIgnoreCase("y"))
            {
                flag = false;
            }
        }
    }
}
