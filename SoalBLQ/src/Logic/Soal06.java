package Logic;

import java.util.Scanner;

public class Soal06 {

    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("6. Palindrome");
        System.out.print("Masukkan data untuk mengecek Palindrome atau tidak : ");
        String data = input.nextLine().toLowerCase();

        char[] dataCharArray = data.toCharArray();
        char[] dataCharArrayRevesed = new char[dataCharArray.length];
        int helper = 0;

        for (int i = dataCharArray.length; i > 0; i--) {
            dataCharArrayRevesed[helper] = dataCharArray[i-1];
            helper++;
        }
        String dataReversed = new String(dataCharArrayRevesed);

        if (!dataReversed.equals(data))
        {
            System.out.println("TIDAK Palindrome");
        }
        else
        {
            System.out.println("Palindrome");
        }
    }
}
