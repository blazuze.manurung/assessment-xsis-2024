package Logic;

import java.util.Scanner;

public class Soal13 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("13. Sudut terkecil jarum jam");
        System.out.println("Format input : 03:00 tidak lebih dari jam 12.00");
        System.out.print("Masukkan jam : ");
        String inputJam = input.nextLine();

        String[] inputJamArray = inputJam.split(":");
        double jam = Double.parseDouble(inputJamArray[0]);
        double menit = Double.parseDouble(inputJamArray[1]);
        double arahMenit = (menit/60) * 12;
        double nilaiSudutPerJam = 30;

        double output = 0;
        if (menit > 0){
            jam += (menit / 60);
        }
        if (jam >= 12){
            jam -= 12;
        }

        if (jam > arahMenit){
            output = (jam - arahMenit) * nilaiSudutPerJam;
        } else if (jam < arahMenit) {
            output = (arahMenit - jam) * nilaiSudutPerJam;
        }

        if (output > 180){
            output = 360 - output;
        }

        System.out.println("Jam " + inputJam + " -> " + (int)output + " Derajat");
    }
}
