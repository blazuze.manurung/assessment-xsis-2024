
package Logic;

import java.util.Arrays;
import java.util.Scanner;

public class Soal07 {

    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("7. Mean, Median, Modus");
        System.out.print("Tolong Masukkan Deret Angka : ");
        String text = input.nextLine().trim();

        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray [i] = Integer.parseInt(textArray[i]);
        }

        double jumlah = 0 , mean = 0, median = 0;
        int modus = 0, helper = 0, jumlahSama = 0, jumlahModus = 0;
        boolean genap;

        if (intArray.length % 2 == 0){
            genap = true;
        }
        else {
            genap = false;
        }

        for (int i = 0; i < intArray.length; i++) {
            // MEAN
            jumlah += intArray[i];
            mean = jumlah / intArray.length;

            // MEDIAN
            if (genap)
            {
                median = (double) (intArray[intArray.length / 2]  + intArray[(intArray.length / 2) - 1]) / 2;
            }
            else {
                median = intArray[intArray.length/2];
            }

            // MODUS
            for (int j = i+1; j < intArray.length; j++) {
                if (intArray[i] < intArray[j]){
                    helper = intArray[i];
                    intArray[i] = intArray[j];
                    intArray[j] = helper;
                }
            }

            for (int j = 0; j < intArray.length; j++) {
                if (intArray[i] == intArray[j] && i != j){
                    jumlahSama++;
                }
            }

            if (jumlahSama >= jumlahModus){
                jumlahModus = jumlahSama;
                modus = intArray[i];
                jumlahSama = 0;
            }
        }

        System.out.println("Mean   : " + mean);
        System.out.println("Median : " + median);
        System.out.println("Modus  : " + modus);

    }
}
